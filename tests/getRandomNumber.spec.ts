import { getRandomNumber } from '../src/utils/getRandomNumber';

describe('getRandomNumber', () => {
  it('returns a random number between 1 and 100 inclusive)', () => {
    const randomNumber = getRandomNumber();
    expect(randomNumber).toBeGreaterThanOrEqual(1);
    expect(randomNumber).toBeLessThanOrEqual(100);
  });
});
