import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import { getCurrentDate } from './utils/getCurrentDate';
import { getRandomNumber } from './utils/getRandomNumber';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/', (req: Request, res: Response) => {
  res.send(
    `Welcome! Your random number: ${getRandomNumber()}. Current date: ${getCurrentDate()}`,
  );
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
